;
;

def a() print(0) end
undef a ; # remove prev def

# now we redefine
def a(a)
    return a + 1; end

def b(a, b, c)
if ((((a && b)) || c))
print(a, b, c);
end end

def loopy(a)
    b = 20
    while(a <= 5)
        if (false)
            0;
        elsif (!(a == 4))
            print(a)
        else
            print(a*1000)
        end
        a+=20
        a = a + 1
        a += -b
        true
    end
end

def loopy2(l)
    until (l == 10)
        unless (l * 10 <= 70) then
            return 99999999
        else
            case (l)
            when (1) then print(101); l += 1;
            when (3) then print(303); l += 1;
            when (5) then print(505); l += 1;
            else
                l &&= l + (40-39);
            end
        end
    end
end

a = 10
print(a(a))
b(1, false, 20)
loopy(1)
print(loopy2(2))