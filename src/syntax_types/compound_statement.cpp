#include "syntax_types.hpp"
#include <iostream>

std::pair<std::optional<Literal*>, ContextTable>
HeadCompoundStatement::interpret(ContextTable&& context) const
{
    auto pair         = this->head_statement->interpret(std::move(context));
    auto return_value = pair.first;
    context           = std::move(pair.second);
    // only continue execution if there was no ``return`` in the previous
    // statements
    if (this->other_statements && !return_value.has_value())
    {
        auto pair2   = this->other_statements->interpret(std::move(context));
        return_value = pair2.first;
        context      = std::move(pair2.second);
    }
    return std::make_pair(return_value, std::move(context));
}

std::pair<std::optional<Literal*>, ContextTable>
LastCompoundStatement::interpret(ContextTable&& context) const
{
    std::optional<Literal*> return_value{};
    if (this->middle_statements)
    {
        auto pair    = this->middle_statements->interpret(std::move(context));
        return_value = pair.first;
        context      = std::move(pair.second);
    }
    // only continue execution if there was no ``return`` in the previous
    // statements
    if (!return_value.has_value())
    {
        auto pair    = this->last_statement->interpret(std::move(context));
        return_value = pair.first;
        context      = std::move(pair.second);
    }
    return std::make_pair(return_value, std::move(context));
}

std::string HeadCompoundStatement::stringify(std::string&& accum) const
{
    accum = this->head_statement->stringify(std::move(accum));
    accum += ";\n";
    if (this->other_statements)
        return this->other_statements->stringify(std::move(accum));
    return std::move(accum);
}

std::string LastCompoundStatement::stringify(std::string&& accum) const
{
    if (this->middle_statements)
        accum = this->middle_statements->stringify(std::move(accum));
    accum = this->last_statement->stringify(std::move(accum));
    return accum + ";\n";
}