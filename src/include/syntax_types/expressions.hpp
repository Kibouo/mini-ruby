#pragma once

struct Expressions
{
    Expression* const  expression;
    Expressions* const other_expressions;
    inline Expressions(Expression* const  expression,
                       Expressions* const other_expressions)
        : expression(expression), other_expressions(other_expressions)
    {
    }
    inline ~Expressions()
    {
        delete this->expression;
        if (this->other_expressions) delete this->other_expressions;
    }
    std::pair<std::vector<std::optional<Literal*>>, ContextTable> interpret(
        ContextTable&&                         context,
        std::vector<std::optional<Literal*>>&& params = {});
    std::string stringify(std::string&& accum = "") const;
};