#pragma once

struct Statement
{
    inline Statement() {}
    virtual inline ~Statement() {}
    virtual std::pair<std::optional<Literal*>, ContextTable> interpret(
        ContextTable&& context) const                             = 0;
    virtual std::string stringify(std::string&& accum = "") const = 0;
};

struct UndefStatement : Statement
{
    Identifier const* const id;
    inline UndefStatement(Identifier const* const id) : id(id) {}
    inline ~UndefStatement() { delete this->id; }
    std::pair<std::optional<Literal*>, ContextTable> interpret(
        ContextTable&& context) const;
    std::string stringify(std::string&& accum = "") const;
};

struct DefStatement : Statement
{
    Identifier const* const        id;
    ArgList const* const           arg_list;
    CompoundStatement const* const compound_statement;
    inline DefStatement(Identifier const* const        id,
                        ArgList const* const           arg_list,
                        CompoundStatement const* const compound_statement)
        : id(id), arg_list(arg_list), compound_statement(compound_statement)
    {
    }
    inline ~DefStatement()
    {
        delete this->id;
        delete this->compound_statement;
        if (this->arg_list) delete this->arg_list;
        for (auto const& literal : this->newly_assigned_literals)
            if (literal) delete literal;
    }
    std::pair<std::optional<Literal*>, ContextTable> interpret(
        ContextTable&& context) const;
    std::pair<std::optional<Literal*>, ContextTable> eval(
        ContextTable&&                         context,
        std::vector<std::optional<Literal*>>&& params = {});
    size_t      arg_list_length() const;
    std::string stringify(std::string&& accum = "") const;

    private:
    std::vector<Literal*> newly_assigned_literals{};
};

struct ReturnStatement : Statement
{
    Expression* const expr;
    inline ReturnStatement(Expression* const expr) : expr(expr) {}
    inline ~ReturnStatement() { delete expr; }
    std::pair<std::optional<Literal*>, ContextTable> interpret(
        ContextTable&& context) const;
    std::string stringify(std::string&& accum = "") const;
};

struct IfStatement : Statement
{
    Expression* const              condition;
    CompoundStatement const* const statement;
    ElsifConditionAndStatements*   elsif_conditions_and_statements;
    CompoundStatement const* const else_statement;
    inline IfStatement(
        Expression* const              condition,
        CompoundStatement const* const statement,
        ElsifConditionAndStatements*   elsif_conditions_and_statements,
        CompoundStatement const* const else_statement)
        : condition(condition)
        , statement(statement)
        , elsif_conditions_and_statements(elsif_conditions_and_statements)
        , else_statement(else_statement)
    {
    }
    inline ~IfStatement()
    {
        delete this->condition;
        delete this->statement;
        if (this->elsif_conditions_and_statements)
            delete this->elsif_conditions_and_statements;
        if (this->else_statement) delete this->else_statement;
    }
    std::pair<std::optional<Literal*>, ContextTable> interpret(
        ContextTable&& context) const;
    std::string stringify(std::string&& accum = "") const;
};

struct UnlessStatement : Statement
{
    Expression* const              condition;
    CompoundStatement const* const statement;
    CompoundStatement const* const else_statement;
    inline UnlessStatement(Expression* const              condition,
                           CompoundStatement const* const statement,
                           CompoundStatement const* const else_statement)
        : condition(condition)
        , statement(statement)
        , else_statement(else_statement)
    {
    }
    inline ~UnlessStatement()
    {
        delete this->condition;
        delete this->statement;
        if (this->else_statement) delete this->else_statement;
    }
    std::pair<std::optional<Literal*>, ContextTable> interpret(
        ContextTable&& context) const;
    std::string stringify(std::string&& accum = "") const;
};

struct WhileStatement : Statement
{
    Expression* const              condition;
    CompoundStatement const* const statement;
    inline WhileStatement(Expression* const              condition,
                          CompoundStatement const* const statement)
        : condition(condition), statement(statement)
    {
    }
    inline ~WhileStatement()
    {
        delete this->condition;
        delete this->statement;
    }
    std::pair<std::optional<Literal*>, ContextTable> interpret(
        ContextTable&& context) const;
    std::string stringify(std::string&& accum = "") const;
};

struct UntilStatement : Statement
{
    Expression* const              condition;
    CompoundStatement const* const statement;
    inline UntilStatement(Expression* const              condition,
                          CompoundStatement const* const statement)
        : condition(condition), statement(statement)
    {
    }
    inline ~UntilStatement()
    {
        delete this->condition;
        delete this->statement;
    }
    std::pair<std::optional<Literal*>, ContextTable> interpret(
        ContextTable&& context) const;
    std::string stringify(std::string&& accum = "") const;
};

struct CaseStatement : Statement
{
    Expression* const              case_expr;
    Expression* const              when_condition;
    CompoundStatement const* const when_statement;
    WhenConditionAndStatements const* const
                                   other_when_conditions_and_statements;
    CompoundStatement const* const else_statement;
    inline CaseStatement(Expression* const              case_expr,
                         Expression* const              when_condition,
                         CompoundStatement const* const when_statement,
                         WhenConditionAndStatements const* const
                                                        other_when_conditions_and_statements,
                         CompoundStatement const* const else_statement)
        : case_expr(case_expr)
        , when_condition(when_condition)
        , when_statement(when_statement)
        , other_when_conditions_and_statements(
              other_when_conditions_and_statements)
        , else_statement(else_statement)
    {
    }
    inline ~CaseStatement()
    {
        delete this->case_expr;
        delete this->when_condition;
        delete this->when_statement;
        if (this->other_when_conditions_and_statements)
            delete this->other_when_conditions_and_statements;
        if (this->else_statement) delete this->else_statement;
    }
    std::pair<std::optional<Literal*>, ContextTable> interpret(
        ContextTable&& context) const;
    std::string stringify(std::string&& accum = "") const;
};

struct ExprStatement : Statement
{
    Expression* const expr;
    inline ExprStatement(Expression* const expr) : expr(expr) {}
    inline ~ExprStatement() { delete this->expr; }
    std::pair<std::optional<Literal*>, ContextTable> interpret(
        ContextTable&& context) const;
    std::string stringify(std::string&& accum = "") const;
};