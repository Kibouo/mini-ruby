#pragma once

// For a more complete typing system it would be better to make a map/dict of
// types and their associated fields (and the types thereof). Each type would in
// turn have a map of functions only available to it (again, along with their
// typing info).
enum TypeEnum
{
    BOOL,
    INT
};

struct Literal
{
    inline Literal(TypeEnum const type, int const value)
        : type(type), value(value)
    {
    }
    inline ~Literal() {}
    void     operator=(Literal const& other);
    void     operator+=(Literal const& other);
    void     operator-=(Literal const& other);
    void     operator*=(Literal const& other);
    void     operator/=(Literal const& other);
    void     and_assign_op(Literal const& other);
    void     or_assign_op(Literal const& other);
    Literal* operator+(Literal const& other) const;
    Literal* operator-(Literal const& other) const;
    Literal* operator*(Literal const& other) const;
    Literal* operator/(Literal const& other) const;
    Literal* operator>(Literal const& other) const;
    Literal* operator>=(Literal const& other) const;
    Literal* operator<(Literal const& other) const;
    Literal* operator<=(Literal const& other) const;
    Literal* operator==(Literal const& other) const;
    Literal* operator!=(Literal const& other) const;
    Literal* operator&&(Literal const& other) const;
    Literal* operator||(Literal const& other) const;

    Literal* not_op() const;
    // In Ruby any value that is not `nil` (does not exist in our
    // interpreter) or explicitly `false` evaluates to `true`.
    bool is_true() const;

    std::string stringify(std::string&& accum = "") const;

    private:
    TypeEnum type;
    int      value;
    void     check_type_same_as(Literal const& other) const;
    void     check_type_same_as(TypeEnum const wanted_type) const;
};