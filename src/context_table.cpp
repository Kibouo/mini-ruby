#include "context_table.hpp"

void ContextTable::insert(std::string const name, Literal* const lit)
{
    this->variables.insert_or_assign(name, lit);
}

/// NOTE: Ruby functions are always global, no matter where they are
/// declared/re-assigned. Example:
/// ```Ruby
/// def b()
///   puts "b"
/// end
/// def a()
///   def b()
///     puts "b1"
///   end
/// end
/// b()
/// a()
/// b()
/// ```
/// The above code outputs ``b`` and then ``b1``.
void ContextTable::insert(std::string const name, DefStatement* const obj)
{
    this->functions.insert_or_assign(name, obj);
    if (this->parent_scope) this->parent_scope->insert(name, obj);
}
void ContextTable::remove_fn(std::string const name)
{
    this->functions.erase(name);
    if (this->parent_scope) this->parent_scope->remove_fn(name);
}

Literal* ContextTable::lookup_var(std::string const name) const
{
    try
    {
        return this->variables.at(name);
    }
    catch (std::out_of_range const&)
    {
        throw UndefinedIdentifier("Undefined variable identifier " + name);
    }
}

// Again, functions are global thus we have to search all upper scopes too.
DefStatement* ContextTable::lookup_fn(std::string const name) const
{
    try
    {
        return this->functions.at(name);
    }
    catch (std::out_of_range const&)
    {
        if (this->parent_scope) { return this->parent_scope->lookup_fn(name); }
        throw UndefinedIdentifier("Undefined function identifier " + name);
    }
}
