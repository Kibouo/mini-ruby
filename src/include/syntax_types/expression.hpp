#pragma once

struct Expressions;

struct Expression
{
    inline Expression() {}
    virtual inline ~Expression()
    {
        for (auto const& literal : this->newly_assigned_literals)
        {
            if (literal) delete literal;
        }
    }
    virtual std::pair<std::optional<Literal*>, ContextTable> interpret(
        ContextTable&& context)
        = 0;
    virtual std::string stringify(std::string&& accum = "") const = 0;

    protected:
    std::vector<Literal*> newly_assigned_literals{};
};

// The fallback syntax component is Expression. So we make yacc create this
// struct for us in the syntax tree on an ERROR.
struct SyntaxErrorExpression : Expression
{
    int const nth;
    inline SyntaxErrorExpression(int const nth) : nth(nth) {}
    std::pair<std::optional<Literal*>, ContextTable> interpret(
        ContextTable&& context);
    std::string stringify(std::string&& accum = "") const;
};

struct AssignExpression : Expression
{
    Identifier const* const id;
    AssignOp const* const   op;
    Expression* const       expression;
    inline AssignExpression(Identifier const* const id,
                            AssignOp const* const   op,
                            Expression* const       expression)
        : id(id), op(op), expression(expression)
    {
    }
    inline ~AssignExpression()
    {
        delete this->id;
        delete this->op;
        delete this->expression;
    }
    std::pair<std::optional<Literal*>, ContextTable> interpret(
        ContextTable&& context);
    std::string stringify(std::string&& accum = "") const;
};

struct BinExpression : Expression
{
    Expression* const  left_expression;
    BinOp const* const op;
    Expression* const  right_expression;
    inline BinExpression(Expression* const  left_expression,
                         BinOp const* const op,
                         Expression* const  right_expression)
        : left_expression(left_expression)
        , op(op)
        , right_expression(right_expression)
    {
    }
    inline ~BinExpression()
    {
        delete this->left_expression;
        delete this->op;
        delete this->right_expression;
    }
    std::pair<std::optional<Literal*>, ContextTable> interpret(
        ContextTable&& context);
    std::string stringify(std::string&& accum = "") const;
};

struct NotExpression : Expression
{
    Expression* const expression;
    inline NotExpression(Expression* const expression) : expression(expression)
    {
    }
    inline ~NotExpression() { delete this->expression; }
    std::pair<std::optional<Literal*>, ContextTable> interpret(
        ContextTable&& context);
    std::string stringify(std::string&& accum = "") const;
};

struct LiteralExpression : Expression
{
    Literal* const literal;
    inline LiteralExpression(Literal* const literal) : literal(literal) {}
    inline ~LiteralExpression() { delete this->literal; }
    std::pair<std::optional<Literal*>, ContextTable> interpret(
        ContextTable&& context);
    std::string stringify(std::string&& accum = "") const;
};

struct IdentifiedExpression : Expression
{
    Identifier const* const id;
    inline IdentifiedExpression(Identifier const* const id) : id(id) {}
    inline ~IdentifiedExpression() { delete this->id; }
    std::pair<std::optional<Literal*>, ContextTable> interpret(
        ContextTable&& context);
    std::string stringify(std::string&& accum = "") const;
};

struct NegExpression : Expression
{
    Expression* const expression;
    inline NegExpression(Expression* const expression) : expression(expression)
    {
    }
    inline ~NegExpression() { delete this->expression; }
    std::pair<std::optional<Literal*>, ContextTable> interpret(
        ContextTable&& context);
    std::string stringify(std::string&& accum = "") const;
};

struct IdentifiedBracedExpression : Expression
{
    Identifier const* const id;
    Expressions* const      expressions;
    inline IdentifiedBracedExpression(Identifier const* const id,
                                      Expressions* const      expressions)
        : id(id), expressions(expressions)
    {
    }
    inline ~IdentifiedBracedExpression()
    {
        delete this->id;
        if (this->expressions) delete this->expressions;
    }
    std::pair<std::optional<Literal*>, ContextTable> interpret(
        ContextTable&& context);
    std::string stringify(std::string&& accum = "") const;
};

struct BracedExpression : Expression
{
    Expression* const expression;
    inline BracedExpression(Expression* const expression)
        : expression(expression)
    {
    }
    inline ~BracedExpression() { delete this->expression; }
    std::pair<std::optional<Literal*>, ContextTable> interpret(
        ContextTable&& context);
    std::string stringify(std::string&& accum = "") const;
};