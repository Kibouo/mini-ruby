WARNING_SETTINGS = -Wall -Wextra -Werror -Wno-delete-incomplete
INCLUDE_PATHS = -I../src/include -I./
FLAGS = -std=c++17
DEBUG_FLAGS = -g -fstandalone-debug

all: syntaxer
all: tokenizer
all: interpreter

debug: FLAGS += $(DEBUG_FLAGS)
debug: all

syntaxer:
	mkdir -p build
	cd build && \
	yacc -v ../src/syntaxer/ruby.yacc --defines=ruby_syntaxer.h --output=ruby_syntaxer.c && \
	clang++ $(FLAGS) $(INCLUDE_PATHS) -Wno-delete-incomplete -c ruby_syntaxer.c -o ruby_syntaxer.o

tokenizer: syntaxer
	cd build && \
	flex -o ruby_tokenizer.c ../src/tokenizer/ruby.lex && \
	clang++ $(FLAGS) $(INCLUDE_PATHS) -Wno-delete-incomplete -c ruby_tokenizer.c -o ruby_tokenizer.o

interpreter: syntaxer tokenizer
	cd build && \
	clang++ $(FLAGS) $(INCLUDE_PATHS) $(WARNING_SETTINGS) -c ../src/main.cpp ../src/syntax_types/*.cpp ../src/context_table.cpp && \
	clang++ $(FLAGS) $(INCLUDE_PATHS) $(WARNING_SETTINGS) -o ruby_interpreter *.o

clean:
	rm -rf build
