#include "syntax_types.hpp"
#include <iostream>

std::pair<std::optional<Literal*>, ContextTable> UndefStatement::interpret(
    ContextTable&& context) const
{
    context.remove_fn(this->id);
    return std::make_pair(std::nullopt, std::move(context));
}

// Interpreting a define statement is ambiguous. The 1st time a def stmt is
// encountered it should be added to the known symbols, while calls result in
// the internal statements being executed.
//
// This interpret is used to save the function to then known symbols.
// To execute the function see ``DefStatement::eval``.
std::pair<std::optional<Literal*>, ContextTable> DefStatement::interpret(
    ContextTable&& context) const
{
    context.insert(this->id, (DefStatement*)this);
    return std::make_pair(std::nullopt, std::move(context));
}

// Actually executes a function.
std::pair<std::optional<Literal*>, ContextTable> DefStatement::eval(
    ContextTable&& context, std::vector<std::optional<Literal*>>&& params)
{
    // Make copies of the literals passed as arguments such that the originals
    // are not modified. Aka allowing scoping of params.
    std::vector<Literal*> param_copies{};
    // create table for new scope
    auto fn_context = ContextTable(&context);

    for (auto const& param : params)
    { param_copies.push_back(new Literal(*(param.value()))); }

    if (this->arg_list)
        fn_context = this->arg_list->interpret(
            std::move(fn_context),
            { param_copies.cbegin(), param_copies.cend() });

    auto pair  = this->compound_statement->interpret(std::move(fn_context));
    fn_context = std::move(pair.second);

    // fn is done. Deallocate scoped literals and get upper context again.
    for (auto const& param_cp : param_copies) { delete param_cp; }
    return std::make_pair(pair.first, std::move(*(fn_context.parent_scope)));
}

size_t DefStatement::arg_list_length() const
{
    return this->arg_list ? this->arg_list->length() : 0;
}

std::pair<std::optional<Literal*>, ContextTable> ReturnStatement::interpret(
    ContextTable&& context) const
{
    // If context table has no parent that means someone placed a return
    // statement in the main body/not method body.
    if (context.parent_scope == nullptr)
    {
        std::cerr << "\nERROR: Called return statement outside of method body."
                  << std::endl;
        exit(1);
    }

    return this->expr->interpret(std::move(context));
}

std::pair<std::optional<Literal*>, ContextTable> IfStatement::interpret(
    ContextTable&& context) const
{
    auto       pair = this->condition->interpret(std::move(context));
    auto const expr = pair.first.value();
    context         = std::move(pair.second);

    std::optional<Literal*> return_value{};
    if (expr->is_true())
    {
        auto pair2   = this->statement->interpret(std::move(context));
        return_value = pair2.first;
        context      = std::move(pair2.second);
    }
    else
    {
        auto elsif_executed = false;
        if (this->elsif_conditions_and_statements)
        {
            auto tuple = this->elsif_conditions_and_statements->interpret(
                std::move(context));
            return_value   = std::get<0>(tuple);
            context        = std::move(std::get<1>(tuple));
            elsif_executed = std::get<2>(tuple);
        }
        if (this->else_statement && !elsif_executed)
        {
            auto pair2   = this->else_statement->interpret(std::move(context));
            return_value = pair2.first;
            context      = std::move(pair2.second);
        }
    }

    return std::make_pair(return_value, std::move(context));
}

std::pair<std::optional<Literal*>, ContextTable> UnlessStatement::interpret(
    ContextTable&& context) const
{
    auto       pair = this->condition->interpret(std::move(context));
    auto const expr = pair.first.value();
    context         = std::move(pair.second);

    std::optional<Literal*> return_value{};
    if (!expr->is_true())
    {
        auto pair2   = this->statement->interpret(std::move(context));
        return_value = pair2.first;
        context      = std::move(pair2.second);
    }
    else if (this->else_statement)
    {
        auto pair2   = this->else_statement->interpret(std::move(context));
        return_value = pair2.first;
        context      = std::move(pair2.second);
    }

    return std::make_pair(return_value, std::move(context));
}

std::pair<std::optional<Literal*>, ContextTable> WhileStatement::interpret(
    ContextTable&& context) const
{
    auto pair = this->condition->interpret(std::move(context));
    auto expr = pair.first.value();
    context   = std::move(pair.second);

    std::optional<Literal*> return_value{};
    while (expr->is_true() && !return_value.has_value())
    {
        auto pair2   = this->statement->interpret(std::move(context));
        return_value = pair2.first;
        context      = std::move(pair2.second);

        // Only continue the loop if there was no return statement called
        if (!return_value.has_value())
        {
            auto pair3 = this->condition->interpret(std::move(context));
            expr       = pair3.first.value();
            context    = std::move(pair3.second);
        }
    }

    return std::make_pair(return_value, std::move(context));
}

std::pair<std::optional<Literal*>, ContextTable> UntilStatement::interpret(
    ContextTable&& context) const
{
    auto pair = this->condition->interpret(std::move(context));
    auto expr = pair.first.value();
    context   = std::move(pair.second);

    std::optional<Literal*> return_value{};
    while (!expr->is_true() && !return_value.has_value())
    {
        auto pair2   = this->statement->interpret(std::move(context));
        return_value = pair2.first;
        context      = std::move(pair2.second);

        // Only continue the loop if there was no return statement called
        if (!return_value.has_value())
        {
            auto pair3 = this->condition->interpret(std::move(context));
            expr       = pair3.first.value();
            context    = std::move(pair3.second);
        }
    }

    return std::make_pair(return_value, std::move(context));
}

std::pair<std::optional<Literal*>, ContextTable> CaseStatement::interpret(
    ContextTable&& context) const
{
    auto       pair      = this->case_expr->interpret(std::move(context));
    auto const case_expr = pair.first.value();
    context              = std::move(pair.second);

    auto       pair2     = this->when_condition->interpret(std::move(context));
    auto const when_expr = pair2.first.value();
    context              = std::move(pair2.second);

    std::optional<Literal*> return_value{};
    auto                    case_bool = false;
    try
    {
        auto const comparison = *case_expr == *when_expr;
        case_bool             = comparison->is_true();
        delete comparison;
    }
    catch (const std::invalid_argument& e)
    {
        std::cerr
            << "\nERROR: Failed to perform boolean check in case statement: "
            << e.what() << std::endl;
        exit(1);
    }

    if (case_bool)
    {
        auto pair3   = this->when_statement->interpret(std::move(context));
        return_value = pair3.first;
        context      = std::move(pair3.second);
    }
    else
    {
        bool whens_executed = false;
        if (this->other_when_conditions_and_statements)
        {
            auto tuple = this->other_when_conditions_and_statements->interpret(
                std::move(context), case_expr);
            return_value   = std::get<0>(tuple);
            context        = std::move(std::get<1>(tuple));
            whens_executed = std::get<2>(tuple);
        }
        if (this->else_statement && !whens_executed)
        {
            auto pair3   = this->else_statement->interpret(std::move(context));
            return_value = pair3.first;
            context      = std::move(pair3.second);
        }
    }

    return std::make_pair(return_value, std::move(context));
}

std::pair<std::optional<Literal*>, ContextTable> ExprStatement::interpret(
    ContextTable&& context) const
{
    auto pair = this->expr->interpret(std::move(context));
    return std::make_pair(std::nullopt, std::move(pair.second));
}

std::string UndefStatement::stringify(std::string&& accum) const
{
    return accum + "undef " + this->id;
}

std::string DefStatement::stringify(std::string&& accum) const
{
    accum += "def " + std::string(this->id) + "(";
    if (this->arg_list) { accum = this->arg_list->stringify(std::move(accum)); }
    accum += ")\n";
    accum = this->compound_statement->stringify(std::move(accum));
    return accum + "end";
}

std::string ReturnStatement::stringify(std::string&& accum) const
{
    accum += "return ";
    return this->expr->stringify(std::move(accum));
}

std::string IfStatement::stringify(std::string&& accum) const
{
    accum += "if";
    accum = this->condition->stringify(std::move(accum)) + " ";
    accum += "then ";
    accum = this->statement->stringify(std::move(accum));
    if (this->elsif_conditions_and_statements)
    {
        accum = this->elsif_conditions_and_statements->stringify(
            std::move(accum));
    }
    if (this->else_statement)
    {
        accum += "else ";
        accum = this->else_statement->stringify(std::move(accum));
    }

    return accum + "end";
}

std::string UnlessStatement::stringify(std::string&& accum) const
{
    accum += "unless";
    accum = this->condition->stringify(std::move(accum)) + " ";
    accum += "then ";
    accum = this->statement->stringify(std::move(accum));
    if (this->else_statement)
    {
        accum += "else ";
        accum = this->else_statement->stringify(std::move(accum));
    }
    return accum + "end";
}

std::string WhileStatement::stringify(std::string&& accum) const
{
    accum += "while";
    accum = this->condition->stringify(std::move(accum)) + " ";
    accum += "do ";
    accum = this->statement->stringify(std::move(accum));
    return accum + "end";
}

std::string UntilStatement::stringify(std::string&& accum) const
{
    accum += "until";
    accum = this->condition->stringify(std::move(accum)) + " ";
    accum += "do ";
    accum = this->statement->stringify(std::move(accum));
    return accum + "end";
}

std::string CaseStatement::stringify(std::string&& accum) const
{
    accum += "case";
    accum = this->case_expr->stringify(std::move(accum));
    accum += "\nwhen";
    accum = this->when_condition->stringify(std::move(accum)) + " ";
    accum += "then ";
    accum = this->when_statement->stringify(std::move(accum));
    if (this->other_when_conditions_and_statements)
    {
        accum = this->other_when_conditions_and_statements->stringify(
            std::move(accum));
    }
    if (this->else_statement)
    {
        accum += "else ";
        accum = this->else_statement->stringify(std::move(accum));
    }
    return accum + "end";
}

std::string ExprStatement::stringify(std::string&& accum) const
{
    return this->expr->stringify(std::move(accum));
}
