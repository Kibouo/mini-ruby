a = 13_45_6 #underscores are allowed in-between digits
R8_u_ = 788 #allowed
_ = 11123   #allowed
a = 8_      #not allowed
8a = 6      #not allowed
& = 7       #not allowed
print(_)
print(R8_u_)
print(a)

