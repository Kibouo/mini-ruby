#include "syntax_types.hpp"
#include <iostream>

Literal* BinOp::interpret(Literal* const       val_l,
                          Literal const* const val_r) const
{
    try
    {
        switch (this->op)
        {
            case BinOpEnum::B_PLUS: return *val_l + *val_r;
            case BinOpEnum::B_MINUS: return *val_l - *val_r;
            case BinOpEnum::B_MUL: return *val_l * *val_r;
            case BinOpEnum::B_DIV: return *val_l / *val_r;
            case BinOpEnum::B_GT: return *val_l > *val_r;
            case BinOpEnum::B_GE: return *val_l >= *val_r;
            case BinOpEnum::B_LT: return *val_l < *val_r;
            case BinOpEnum::B_LE: return *val_l <= *val_r;
            case BinOpEnum::B_EQ: return *val_l == *val_r;
            case BinOpEnum::B_NE: return *val_l != *val_r;
            case BinOpEnum::B_AND: return *val_l && *val_r;
            case BinOpEnum::B_OR: return *val_l || *val_r;
        }
    }
    catch (const std::invalid_argument& e)
    {
        throw e;
    }
}

std::string BinOp::stringify(std::string&& accum) const
{
    switch (this->op)
    {
        case BinOpEnum::B_PLUS: return accum += "+";
        case BinOpEnum::B_MINUS: return accum += "-";
        case BinOpEnum::B_MUL: return accum += "*";
        case BinOpEnum::B_DIV: return accum += "/";
        case BinOpEnum::B_GT: return accum += ">";
        case BinOpEnum::B_GE: return accum += ">=";
        case BinOpEnum::B_LT: return accum += "<";
        case BinOpEnum::B_LE: return accum += "<=";
        case BinOpEnum::B_EQ: return accum += "==";
        case BinOpEnum::B_NE: return accum += "!=";
        case BinOpEnum::B_AND: return accum += "&&";
        case BinOpEnum::B_OR: return accum += "||";
    }
}