#include "syntax_types.hpp"
#include <iostream>

std::pair<std::vector<std::optional<Literal*>>, ContextTable>
Expressions::interpret(ContextTable&&                         context,
                       std::vector<std::optional<Literal*>>&& params)
{
    auto pair = this->expression->interpret(std::move(context));
    params.push_back(pair.first);
    context = std::move(pair.second);

    if (this->other_expressions)
    {
        auto pair2 = this->other_expressions->interpret(std::move(context),
                                                        std::move(params));
        params     = std::move(pair2.first);
        context    = std::move(pair2.second);
    }

    return std::make_pair(std::move(params), std::move(context));
}

std::string Expressions::stringify(std::string&& accum) const
{
    accum = this->expression->stringify(std::move(accum));
    if (this->other_expressions)
    {
        accum += ", ";
        accum = this->other_expressions->stringify(std::move(accum));
    }
    return std::move(accum);
}