#include "syntax_types.hpp"
#include <iostream>

bool Literal::is_true() const
{
    return !(this->value == 0 && this->type == TypeEnum::BOOL);
}

void Literal::check_type_same_as(Literal const& other) const
{
    if (this->type != other.type)
        throw std::invalid_argument("Types do not match.");
}
void Literal::check_type_same_as(TypeEnum const wanted_type) const
{
    if (this->type != wanted_type)
        throw std::invalid_argument("Invalid type for operator.");
}

void Literal::operator=(Literal const& other)
{
    this->type  = other.type;
    this->value = other.value;
}
void Literal::operator+=(Literal const& other)
{
    this->check_type_same_as(other);
    this->check_type_same_as(TypeEnum::INT);
    this->value += other.value;
}
void Literal::operator-=(Literal const& other)
{
    this->check_type_same_as(other);
    this->check_type_same_as(TypeEnum::INT);
    this->value -= other.value;
}
void Literal::operator*=(Literal const& other)
{
    this->check_type_same_as(other);
    this->check_type_same_as(TypeEnum::INT);
    this->value *= other.value;
}
void Literal::operator/=(Literal const& other)
{
    this->check_type_same_as(other);
    this->check_type_same_as(TypeEnum::INT);
    if (other.value == 0)
    { throw std::invalid_argument("Tried to divide by 0"); }

    this->value /= other.value;
}
void Literal::and_assign_op(Literal const& other)
{
    if (this->is_true()) this->value = other.value;
}
void Literal::or_assign_op(Literal const& other)
{
    if (!this->is_true()) this->value = other.value;
}

Literal* Literal::operator+(Literal const& other) const
{
    this->check_type_same_as(TypeEnum::INT);
    this->check_type_same_as(other);
    return new Literal(TypeEnum::INT, this->value + other.value);
}
Literal* Literal::operator-(Literal const& other) const
{
    this->check_type_same_as(TypeEnum::INT);
    this->check_type_same_as(other);
    return new Literal(TypeEnum::INT, this->value - other.value);
}
Literal* Literal::operator*(Literal const& other) const
{
    this->check_type_same_as(TypeEnum::INT);
    this->check_type_same_as(other);
    return new Literal(TypeEnum::INT, this->value * other.value);
}
Literal* Literal::operator/(Literal const& other) const
{
    this->check_type_same_as(TypeEnum::INT);
    this->check_type_same_as(other);
    if (other.value == 0)
    { throw std::invalid_argument("Tried to divide by 0"); }

    return new Literal(TypeEnum::INT, this->value / other.value);
}
Literal* Literal::operator>(Literal const& other) const
{
    this->check_type_same_as(TypeEnum::INT);
    this->check_type_same_as(other);
    return new Literal(TypeEnum::BOOL, this->value > other.value ? 1 : 0);
}
Literal* Literal::operator>=(Literal const& other) const
{
    this->check_type_same_as(TypeEnum::INT);
    this->check_type_same_as(other);
    return new Literal(TypeEnum::BOOL, this->value >= other.value ? 1 : 0);
}
Literal* Literal::operator<(Literal const& other) const
{
    this->check_type_same_as(TypeEnum::INT);
    this->check_type_same_as(other);
    return new Literal(TypeEnum::BOOL, this->value < other.value ? 1 : 0);
}
Literal* Literal::operator<=(Literal const& other) const
{
    this->check_type_same_as(TypeEnum::INT);
    this->check_type_same_as(other);
    return new Literal(TypeEnum::BOOL, this->value <= other.value ? 1 : 0);
}
Literal* Literal::operator==(Literal const& other) const
{
    this->check_type_same_as(other);
    return new Literal(TypeEnum::BOOL, this->value == other.value ? 1 : 0);
}
Literal* Literal::operator!=(Literal const& other) const
{
    this->check_type_same_as(other);
    return new Literal(TypeEnum::BOOL, this->value != other.value ? 1 : 0);
}
Literal* Literal::operator&&(Literal const& other) const
{
    return new Literal(TypeEnum::BOOL,
                       this->is_true() && other.is_true() ? 1 : 0);
}
Literal* Literal::operator||(Literal const& other) const
{
    return new Literal(TypeEnum::BOOL,
                       this->is_true() || other.is_true() ? 1 : 0);
}

Literal* Literal::not_op() const
{
    return new Literal(TypeEnum::BOOL, !this->is_true() ? 1 : 0);
}

std::string Literal::stringify(std::string&& accum) const
{
    switch (this->type)
    {
        case TypeEnum::INT: return accum + std::to_string(this->value);
        case TypeEnum::BOOL:
            return accum + (this->value > 0 ? "true" : "false");
    }
}