#pragma once

// Using a prefix to prevent clash with the generated tokens enum
// (``yytokentype``). We prefer ``AssignOp`` and ``BinOp`` as separate enums to
// differentiate the 2 groups of operators.
enum AssignOpEnum
{
    A_ASSIGN,
    A_PLUSASSIGN,
    A_MINUSASSIGN,
    A_MULASSIGN,
    A_DIVASSIGN,
    A_ANDASSIGN,
    A_ORASSIGN
};

struct AssignOp
{
    AssignOpEnum const op;
    inline AssignOp(AssignOpEnum const op) : op(op) {}
    Literal* interpret(Literal* const val_l, Literal const* const val_r) const;
    std::string stringify(std::string&& accum = "") const;

    bool is_simple_assign() const { return this->op == AssignOpEnum::A_ASSIGN; }
};
