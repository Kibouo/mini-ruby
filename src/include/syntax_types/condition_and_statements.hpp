#pragma once

struct Expression;

// This struct is a part of other syntax types. It is used as part
// of conditional statements for better structured code.
struct ConditionAndStatement
{
    Expression* const                  condition;
    CompoundStatement const* const     statement;
    ConditionAndStatement const* const other_conditions_and_statements;
    inline ConditionAndStatement(
        Expression* const                  condition,
        CompoundStatement const* const     statement,
        ConditionAndStatement const* const other_conditions_and_statements)
        : condition(condition)
        , statement(statement)
        , other_conditions_and_statements(other_conditions_and_statements)
    {
    }
    virtual inline ~ConditionAndStatement()
    {
        delete this->condition;
        delete this->statement;
        if (this->other_conditions_and_statements)
            delete this->other_conditions_and_statements;
    }
    virtual std::tuple<std::optional<Literal*>, ContextTable, bool> interpret(
        ContextTable&&       context,
        Literal const* const case_expr = nullptr) const           = 0;
    virtual std::string stringify(std::string&& accum = "") const = 0;
};

struct ElsifConditionAndStatements : ConditionAndStatement
{
    inline ElsifConditionAndStatements(Expression* const              condition,
                                       CompoundStatement const* const statement,
                                       ElsifConditionAndStatements const* const
                                           other_conditions_and_statements)
        : ConditionAndStatement(
            condition, statement, other_conditions_and_statements)
    {
    }
    std::tuple<std::optional<Literal*>, ContextTable, bool> interpret(
        ContextTable&& context, Literal const* const case_expr = nullptr) const;
    std::string stringify(std::string&& accum = "") const;
};

struct WhenConditionAndStatements : ConditionAndStatement
{
    inline WhenConditionAndStatements(
        Expression* const                       condition,
        CompoundStatement const* const          statement,
        WhenConditionAndStatements const* const other_conditions_and_statements)
        : ConditionAndStatement(
            condition, statement, other_conditions_and_statements)
    {
    }
    std::tuple<std::optional<Literal*>, ContextTable, bool> interpret(
        ContextTable&& context, Literal const* const case_expr) const;
    std::string stringify(std::string&& accum = "") const;
};
