%{
    #include <stdio.h>
    #include "syntax_types.hpp"
    #include <iostream>

    Program* program = NULL;
    extern "C++" int yylex();

    /// error reporting
    // Array with tokens such that index = tokenid - 258
    std::string const tokens[]
        = { "IDENTIFIER", "BOOLEAN",   "INTEGER",    "SEMICOLON",   "UNDEF",
            "DEF",        "LPAREN",    "RPAREN",     "END",         "RETURN",
            "IF",         "THEN",      "ELSIF",      "ELSE",        "UNLESS",
            "WHILE",      "DO",        "UNTIL",      "CASE",        "WHEN",
            "COMMA",      "ASSIGN",    "PLUSASSIGN", "MINUSASSIGN", "MULASSIGN",
            "DIVASSIGN",  "ANDASSIGN", "ORASSIGN",   "PLUS",        "MINUS",
            "MUL",        "DIV",       "GT",         "GE",          "LT",
            "LE",         "EQ",        "NE",         "AND",         "OR",
            "NOT",        "UMINUS" };

    std::string const token_to_string(int const token_nr) {
        return tokens[token_nr - 258];
    }

    int error_count = 0;
    // position of the token where error happened (parser already advanced the actual nrs)
    extern "C++" int line_nr;
    extern "C++" int col_nr;
    // lookahead token
    extern "C++" int yychar;
    void yyerror(const char *s) { 
        error_count += 1;

        // Special case. Happens at the end of files when e.g. an `if` statement is not completed 
        // by adding the `end` keyword.
        if (yychar == 0) {
            std::cerr << error_count << ") " << s << " @(" << line_nr << ", " 
            << col_nr << "): expected token to finish previous statement." << std::endl;
        }
        else {
            std::cerr << error_count << ") " << s << " @(" << line_nr << ", " 
                << col_nr << "): unexpected token '" 
                << token_to_string(yychar) << "'." << std::endl;
        }
    }
%}

%union
{
    int integer_t;
    bool boolean_t;
    char* string_t;
    struct Program* program_t;
    struct CompoundStatement* compound_statement_t; 
    struct Statement* statement_t; 
    struct Expression* expression_t; 
    struct Literal* literal_t; 
    struct Expressions* expressions_t; 
    struct ArgList* arg_list_t; 
    struct ElsifConditionAndStatements* elsif_t;
    struct WhenConditionAndStatements* when_t;
}
%type <program_t> program
%type <compound_statement_t> compound_statement
%type <statement_t> statement
%type <expression_t> expression
%type <literal_t> literal
%type <expressions_t> expressions
%type <arg_list_t> arg_list
%type <compound_statement_t> a
%type <arg_list_t> c
%type <elsif_t> d
%type <compound_statement_t> e
%type <when_t> f
%type <expressions_t> g
%type <expressions_t> h
%type <arg_list_t> i
%token <string_t> IDENTIFIER
%token <boolean_t> BOOLEAN
%token <integer_t> INTEGER
%token
    SEMICOLON UNDEF DEF LPAREN RPAREN END RETURN IF THEN ELSIF ELSE UNLESS WHILE DO UNTIL CASE WHEN COMMA ASSIGN PLUSASSIGN MINUSASSIGN MULASSIGN DIVASSIGN ANDASSIGN ORASSIGN PLUS MINUS MUL DIV GT GE LT LE EQ NE AND OR NOT
%right IF UNLESS WHILE UNTIL
%right ASSIGN MULASSIGN DIVASSIGN PLUSASSIGN MINUSASSIGN ANDASSIGN ORASSIGN
%left OR
%left AND
%right EQ NE
%left LT LE GT GE
%left PLUS MINUS
%left MUL DIV
%right UMINUS
%right NOT

%%
program             : compound_statement { $$ = new Program($1); program = $$; }
                    ;

compound_statement  : statement a b { $$ = new HeadCompoundStatement($1, $2); }
                    ;

a                   : epsilon { $$ = NULL; }
                    | a terminator statement { $$ = new LastCompoundStatement($1, $3); }  
                    ;

b                   : epsilon
                    | terminator        
                    ;

statement           : UNDEF IDENTIFIER { $$ = new UndefStatement($2); }
                    | DEF IDENTIFIER LPAREN c RPAREN b compound_statement END { 
                        $$ = new DefStatement($2, $4, $7); 
                    }
                    | RETURN expression { $$ = new ReturnStatement($2); }
                    | IF expression then compound_statement d e END {
                        $$ = new IfStatement($2, $4, $5, $6);
                    }
                    | UNLESS expression then compound_statement e END {
                        $$ = new UnlessStatement($2, $4, $5);
                    }
                    | WHILE expression do compound_statement END { 
                        $$ = new WhileStatement($2, $4);
                    }
                    | UNTIL expression do compound_statement END { 
                        $$ = new UntilStatement($2, $4);
                    }
                    | CASE expression b
                    WHEN expression then compound_statement
                    f e END {
                        $$ = new CaseStatement($2, $5, $7, $8, $9);
                    }
                    | expression { $$ = new ExprStatement($1); }
                    ;

c                   : epsilon { $$ = NULL; }
                    | arg_list { $$ = $1; }
                    ;

d                   : epsilon { $$ = NULL; }
                    | ELSIF expression then compound_statement d {
                        $$ = new ElsifConditionAndStatements($2, $4, $5);
                    }
                    ;

e                   : epsilon { $$ = NULL; }
                    | ELSE b compound_statement { $$ = $3; }
                    ;

f                   : epsilon { $$ = NULL; }
                    | WHEN expression then compound_statement f {
                        $$ = new WhenConditionAndStatements($2, $4, $5);
                    }
                    ;

expression          : IDENTIFIER ASSIGN expression {
                        // Initially each bin & assign operator was placed, per category, in a 
                        // separate group. However, yacc could not resolve shift/reduce conflicts 
                        // that way even with precedence hints. Hence the copies for each operator.
                        $$ = new AssignExpression($1, new AssignOp(AssignOpEnum::A_ASSIGN), $3); 
                    }
                    | IDENTIFIER PLUSASSIGN expression {
                        $$ = new AssignExpression($1, new AssignOp(AssignOpEnum::A_PLUSASSIGN), $3); 
                    }
                    | IDENTIFIER MINUSASSIGN expression {
                        $$ = new AssignExpression($1, new AssignOp(AssignOpEnum::A_MINUSASSIGN), $3); 
                    }
                    | IDENTIFIER MULASSIGN expression {
                        $$ = new AssignExpression($1, new AssignOp(AssignOpEnum::A_MULASSIGN), $3); 
                    }
                    | IDENTIFIER DIVASSIGN expression {
                        $$ = new AssignExpression($1, new AssignOp(AssignOpEnum::A_DIVASSIGN), $3); 
                    }
                    | IDENTIFIER ANDASSIGN expression {
                        $$ = new AssignExpression($1, new AssignOp(AssignOpEnum::A_ANDASSIGN), $3); 
                    }
                    | IDENTIFIER ORASSIGN expression {
                        $$ = new AssignExpression($1, new AssignOp(AssignOpEnum::A_ORASSIGN), $3); 
                    }
                    | expression PLUS expression { 
                        $$ = new BinExpression($1, new BinOp(BinOpEnum::B_PLUS), $3); 
                    }
                    | expression MINUS expression { 
                        $$ = new BinExpression($1, new BinOp(BinOpEnum::B_MINUS), $3); 
                    }
                    | expression MUL expression { 
                        $$ = new BinExpression($1, new BinOp(BinOpEnum::B_MUL), $3); 
                    }
                    | expression DIV expression { 
                        $$ = new BinExpression($1, new BinOp(BinOpEnum::B_DIV), $3); 
                    }
                    | expression GT expression { 
                        $$ = new BinExpression($1, new BinOp(BinOpEnum::B_GT), $3); 
                    }
                    | expression GE expression { 
                        $$ = new BinExpression($1, new BinOp(BinOpEnum::B_GE), $3); 
                    }
                    | expression LT expression { 
                        $$ = new BinExpression($1, new BinOp(BinOpEnum::B_LT), $3); 
                    }
                    | expression LE expression { 
                        $$ = new BinExpression($1, new BinOp(BinOpEnum::B_LE), $3); 
                    }
                    | expression EQ expression { 
                        $$ = new BinExpression($1, new BinOp(BinOpEnum::B_EQ), $3); 
                    }
                    | expression NE expression { 
                        $$ = new BinExpression($1, new BinOp(BinOpEnum::B_NE), $3); 
                    }
                    | expression AND expression { 
                        $$ = new BinExpression($1, new BinOp(BinOpEnum::B_AND), $3); 
                    }
                    | expression OR expression { 
                        $$ = new BinExpression($1, new BinOp(BinOpEnum::B_OR), $3); 
                    }
                    | NOT expression { $$ = new NotExpression($2); }
                    | literal { $$ = new LiteralExpression($1); }
                    | IDENTIFIER { $$ = new IdentifiedExpression($1); }
                    | MINUS expression %prec UMINUS { $$ = new NegExpression($2); }
                    | IDENTIFIER LPAREN g RPAREN { $$ = new IdentifiedBracedExpression($1, $3); }
                    | LPAREN expression RPAREN { $$ = new BracedExpression($2); }
                    | error {
                        $$ = new SyntaxErrorExpression(error_count);
                    }
                    ;

literal             : INTEGER { $$ = new Literal(TypeEnum::INT, $1); }
                    | BOOLEAN { $$ = new Literal(TypeEnum::BOOL, $1); }
                    ;

g                   : epsilon { $$ = NULL; }
                    | expressions { $$ = $1; }
                    ;

expressions         : expression h { $$ = new Expressions($1, $2); }
                    ;

h                   : epsilon { $$ = NULL; }
                    | COMMA expression h { $$ = new Expressions($2, $3); }
                    ;

arg_list            : IDENTIFIER i { $$ = new ArgList($1, $2); }
                    ;

i                   : epsilon { $$ = NULL; }
                    | COMMA IDENTIFIER i { $$ = new ArgList($2, $3); }
                    ;

then                : terminator
                    | THEN
                    | terminator THEN
                    | THEN terminator
                    ;

do                  : terminator
                    | DO
                    | terminator DO
                    | DO terminator
                    ;
                    
terminator          : SEMICOLON
                    ;

epsilon             :
                    ;
%%