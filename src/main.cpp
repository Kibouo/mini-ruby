#include "main.hpp"
#include "ruby_syntaxer.h"
#include <iostream>
#include <stdexcept>

void parse_application_arguments(int const argc, char const* const argv[])
{
    for (auto i = 0; i < argc; i++)
    {
        switch (i)
        {
            case 0: break;
            case 1: set_token_source(argv[i]); break;
            default: break;
        }
    }
}

void set_token_source(char const* const file_path)
{
    // If we de not explicitly bind yyin to a file, stdin is assumed.
    auto file = fopen(file_path, "r");
    if (file == NULL)
        throw std::invalid_argument("Invalid file path for token source.");

    yyset_in(file);
    yyrestart(file);
}

void close_token_source() { fclose(yyget_in()); }

std::vector<std::tuple<std::string const, std::string const>> const
parse_tokens()
{
    std::vector<std::tuple<std::string const, std::string const>> parsed_tokens
        = {};
    while (int tokenid = yylex())
    {
        auto const token_type = token_to_string(tokenid);

        // Append value
        auto token_value = "";
        if ((tokenid == IDENTIFIER) || (tokenid == INTEGER)
            || (tokenid == BOOLEAN))
            token_value = yytext;

        parsed_tokens.push_back({ token_type, token_value });
    }

    return parsed_tokens;
}

void print_tokens(
    std::vector<std::tuple<std::string const, std::string const>> const tokens)
{
    for (auto& token : tokens)
    {
        auto token_type  = std::get<0>(token);
        auto token_value = std::get<1>(token);

        std::cout << " " << token_type;
        if (token_value != "") std::cout << "=\"" << token_value << "\"";
    }
    std::cout << "\n";
}
// A program might not even be built by the parser. For example the 1-liner
// `return\n`.
void print_syntaxed_program()
{
    yyparse();
    if (program) { std::cout << program->stringify(); }
}
void interpret_program()
{
    yyparse();
    if (program) { program->interpret(); }
}

int main(int const argc, char const* const argv[])
{
    parse_application_arguments(argc, argv);

    // print_tokens(parse_tokens());
    // print_syntaxed_program();
    interpret_program();

    close_token_source();
    return 0;
}
