#pragma once

struct Program
{
    CompoundStatement const* const compound_statement;
    inline Program(CompoundStatement const* const compound_statement)
        : compound_statement(compound_statement)
    {
    }
    inline ~Program() { delete this->compound_statement; }
    void        interpret() const;
    std::string stringify(std::string&& accum = "") const;
};
