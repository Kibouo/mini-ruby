/* I kept getting the error "undefined reference to `yylex'" during linking. I assume it has something to do with trying to compile in C++ and name mangling between C/C++. Adding this option and removing the link with libfl fixes it for some reason. */
%option noyywrap

%{
#include "syntax_types.hpp"
#include "ruby_syntaxer.h"

// Keep track of current position of lex for error messages
int line_nr = 1;
int col_nr = 1;

bool empty_line = true;

void cursor_add_token(bool only_whitespace = false) {
    // && -> prevent reset due to whitespace after actual code
    empty_line = empty_line && only_whitespace; 
    col_nr += yyleng; 
}
void cursor_move_newline(int amt) {
    line_nr += amt; 
    empty_line = true;
    if(amt > 0) {
        col_nr = 1;
    }
}
%}

/* Regex definitions */
newline     \n
semicolon   ";"
comment     "#"[^\n]*
whitespace  (" "|\t|\r|\f)+ 
undef       undef
def         def
end         end
return      return
then        then
elsif       elsif
else        else
if          if
unless      unless
while       while
do          do
until       until
case        case
when        when
boolean     true|false
integer     ([0-9]|[0-9]"_"*[0-9])+
identifier  [A-Za-z_][A-Za-z0-9_]*
lparen      "("
rparen      ")"
comma       ","
plusassign  "+="
minusassign "-="
mulassign   "*="
divassign   "/="
andassign   "&&="
orassign    "||="
ge          ">="
le          "<="
ne          "!="
eq          "=="
plus        "+"
minus       "-"
mul         "*"
div         "/"
and         "&&"
or          "||"
gt          ">"
lt          "<"
not         "!"
assign      "="
other       .

/* Lexer rule declarations */
%%
{newline}+ {
    // cursor_move_newline edits line state
    bool return_semi = !empty_line;
    cursor_move_newline(yyleng);
    if (return_semi) return SEMICOLON;
}
{semicolon}{newline}* {
    // cursor_move_newline edits line state
    bool return_semi = !empty_line;
    cursor_add_token(); 
    cursor_move_newline(yyleng-1); 
    if (return_semi) return SEMICOLON;
}
{comment} { 
    // Don't count chars as nothing after a comment on the same line 
    // can be valid code. A newline will end it and reset col count.
}
{whitespace} { cursor_add_token(true); }
{undef} { cursor_add_token(); return UNDEF; }
{def} { cursor_add_token(); return DEF; }
{end} { cursor_add_token(); return END; }
{return} { cursor_add_token(); return RETURN; }
{then} { cursor_add_token(); return THEN; }
{elsif} { cursor_add_token(); return ELSIF; }
{else}  { cursor_add_token(); return ELSE; }
{if}  { cursor_add_token(); return IF; }
{unless} { cursor_add_token(); return UNLESS; }
{while} { cursor_add_token(); return WHILE; }
{do} { cursor_add_token(); return DO; }
{until} { cursor_add_token(); return UNTIL; }
{case} { cursor_add_token(); return CASE; }
{when} { cursor_add_token(); return WHEN; }
{boolean} { 
    cursor_add_token(); 
    yylval.boolean_t = strcmp(yytext, "true") == 0;
    return BOOLEAN; 
}
{integer} { 
    cursor_add_token(); 
    yylval.integer_t = atoi(yytext);
    return INTEGER; 
}
{identifier} { 
    cursor_add_token(); 
    yylval.string_t = strdup(yytext);
    return IDENTIFIER; 
}
{lparen} { cursor_add_token(); return LPAREN; }
{rparen} { cursor_add_token(); return RPAREN; }
{comma} { cursor_add_token(); return COMMA; }
{plusassign} { cursor_add_token(); return PLUSASSIGN; }
{minusassign} { cursor_add_token(); return MINUSASSIGN; }
{mulassign} { cursor_add_token(); return MULASSIGN; }
{divassign} { cursor_add_token(); return DIVASSIGN; }
{andassign} { cursor_add_token(); return ANDASSIGN; }
{orassign} { cursor_add_token(); return ORASSIGN; }
{ge} { cursor_add_token(); return GE; }
{le} { cursor_add_token(); return LE; }
{ne} { cursor_add_token(); return NE; }
{eq} { cursor_add_token(); return EQ; }
{plus} { cursor_add_token(); return PLUS; }
{minus} { cursor_add_token(); return MINUS; }
{mul} { cursor_add_token(); return MUL; }
{div} { cursor_add_token(); return DIV; }
{and} { cursor_add_token(); return AND; }
{or} { cursor_add_token(); return OR; }
{gt} { cursor_add_token(); return GT; }
{lt} { cursor_add_token(); return LT; }
{not} { cursor_add_token(); return NOT; }
{assign} { cursor_add_token(); return ASSIGN; }
{other} {
    // non-printable char
    if (yytext[0] < ' ')
        fprintf(stderr, "illegal character: ^%c", yytext[0] + '@');
    else {
        // non-printable char printed as octal int padded with zeros, eg \012
        if (yytext[0] > '~')
            fprintf(stderr, "illegal character: \\%03o", (int) yytext[0]);
        else
            fprintf(stderr, "illegal character: %s", yytext);
    }
    // lex read exactly one char; the illegal one
    fprintf(stderr, " at line %d column %d\n", line_nr, col_nr);

    cursor_add_token();
}
%%