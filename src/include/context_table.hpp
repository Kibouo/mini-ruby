#pragma once

#include <exception>
#include <string>
#include <unordered_map>

struct Literal;
struct DefStatement;

struct UndefinedIdentifier : std::runtime_error
{
    inline UndefinedIdentifier(std::string const& reason)
        : std::runtime_error(reason)
    {
    }
};

struct ContextTable
{
    inline ContextTable(ContextTable* parent_scope = nullptr)
        : parent_scope(parent_scope)
    {
    }

    void          insert(std::string const name, Literal* const obj);
    void          insert(std::string const name, DefStatement* const obj);
    void          remove_fn(std::string const name);
    Literal*      lookup_var(std::string const name) const;
    DefStatement* lookup_fn(std::string const name) const;
    ContextTable* parent_scope = nullptr;

    // dissable copy constructor but keep std::move constructor
    ContextTable(ContextTable&)       = delete;
    ContextTable(ContextTable const&) = delete;
    ContextTable(ContextTable&&)      = default;
    ContextTable& operator=(ContextTable&& other) = default;

    private:
    std::unordered_map<std::string, Literal*>      variables{};
    std::unordered_map<std::string, DefStatement*> functions{};
};