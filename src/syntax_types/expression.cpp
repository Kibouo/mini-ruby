#include "syntax_types.hpp"
#include <cstring>
#include <iostream>

struct SyntaxError : std::runtime_error
{
    inline SyntaxError(std::string const& reason) : std::runtime_error(reason)
    {
    }
};

std::pair<std::optional<Literal*>, ContextTable>
SyntaxErrorExpression::interpret(ContextTable&& context)
{
    // Specific messages are given by yacc's ``yyerror``.
    std::cerr << "\nERROR: hit syntax error nr. " << this->nth << std::endl;
    exit(1);

    // dead code due to exit
    return { {}, std::move(context) };
}

std::pair<std::optional<Literal*>, ContextTable> AssignExpression::interpret(
    ContextTable&& context)
{
    auto       pair = this->expression->interpret(std::move(context));
    auto const expr = pair.first.value();
    context         = std::move(pair.second);

    Literal* orig_value = nullptr;
    try
    {
        orig_value = context.lookup_var(this->id);
    }
    catch (const UndefinedIdentifier& e)
    {
        // for simple assignments the left hand side will possibly be a
        // non-existent variable name. This is the only acceptable case for
        // missing ids.
        if (this->op->is_simple_assign())
        {
            // copy to keep type
            orig_value = new Literal(*expr);
            this->newly_assigned_literals.push_back(orig_value);
        }
        else
        {
            std::cerr << "\nERROR: ";
            std::cerr << e.what() << std::endl;
            exit(1);
        }
    }

    Literal* result = nullptr;
    try
    {
        result = this->op->interpret(orig_value, expr);
    }
    catch (const std::invalid_argument& e)
    {
        std::cerr << "\nERROR: Failed assign expression:" << std::endl;
        std::cerr << this->stringify() << std::endl;
        std::cerr << e.what() << std::endl;
        exit(1);
    }

    context.insert(this->id, result);

    return std::make_pair(result, std::move(context));
}

std::pair<std::optional<Literal*>, ContextTable> BinExpression::interpret(
    ContextTable&& context)
{
    auto       pair      = this->left_expression->interpret(std::move(context));
    auto const left_expr = pair.first.value();
    context              = std::move(pair.second);
    auto       pair2 = this->right_expression->interpret(std::move(context));
    auto const right_expr = pair2.first.value();
    context               = std::move(pair2.second);

    Literal* result = nullptr;
    try
    {
        result = this->op->interpret(left_expr, right_expr);
    }
    catch (const std::invalid_argument& e)
    {
        std::cerr << "\nERROR: Failed binary expression:" << std::endl;
        std::cerr << this->stringify() << std::endl;
        std::cerr << e.what() << std::endl;
        exit(1);
    }
    this->newly_assigned_literals.push_back(result);

    return std::make_pair(result, std::move(context));
}

std::pair<std::optional<Literal*>, ContextTable> NotExpression::interpret(
    ContextTable&& context)
{
    auto       pair = this->expression->interpret(std::move(context));
    auto const expr = pair.first.value();
    context         = std::move(pair.second);

    auto const result = expr->not_op();
    this->newly_assigned_literals.push_back(result);

    return std::make_pair(result, std::move(context));
}

std::pair<std::optional<Literal*>, ContextTable> LiteralExpression::interpret(
    ContextTable&& context)
{
    return std::make_pair(this->literal, std::move(context));
}

std::pair<std::optional<Literal*>, ContextTable>
IdentifiedExpression::interpret(ContextTable&& context)
{
    std::optional<Literal*> expr{};
    try
    {
        expr = context.lookup_var(this->id);
    }
    // In Ruby, a sole identifier without brackets which is not a variable is
    // equal to a function call without parameters. We do not check whether this
    // identifier is the 'print' function as
    // ``IdentifiedExpression`` has no arguments and printing without arguments
    // does not do anything.
    catch (UndefinedIdentifier const& var_e)
    {
        DefStatement* function = nullptr;
        try
        {
            function = context.lookup_fn(this->id);
        }
        catch (UndefinedIdentifier const& fn_e)
        {
            std::cerr << "\nERROR: ";
            std::cerr << var_e.what() << std::endl;
            std::cerr << fn_e.what() << std::endl;
            exit(1);
        }

        if (function->arg_list_length() != 0)
        {
            std::cerr << "\nERROR: Unable to call function "
                             + std::string(function->id)
                             + " with 0 arguments. Expected "
                             + std::to_string(function->arg_list_length()) + "."
                      << std::endl;
            exit(1);
        }

        auto pair = function->eval(std::move(context));
        expr      = pair.first.value();
        context   = std::move(pair.second);
    }
    return std::make_pair(expr, std::move(context));
}

std::pair<std::optional<Literal*>, ContextTable> NegExpression::interpret(
    ContextTable&& context)
{
    auto       pair = this->expression->interpret(std::move(context));
    auto const expr = pair.first.value();
    context         = std::move(pair.second);

    Literal* result = nullptr;
    try
    {
        result = Literal(TypeEnum::INT, 0) - *expr;
    }
    catch (const std::invalid_argument& e)
    {
        std::cerr << "\nERROR: Failed to negate literal: " << e.what()
                  << std::endl;
        exit(1);
    }
    this->newly_assigned_literals.push_back(result);

    return std::make_pair(result, std::move(context));
}

std::pair<std::optional<Literal*>, ContextTable>
IdentifiedBracedExpression::interpret(ContextTable&& context)
{
    std::optional<Literal*> result{};

    // ``print`` is a built-in function
    if (strcmp(this->id, "print") == 0)
    {
        // only bother if there are actual arguments to print.
        if (this->expressions)
        {

            auto       pair = this->expressions->interpret(std::move(context));
            auto const literals = pair.first;
            context             = std::move(pair.second);

            for (auto const& literal : literals)
            {
                // Actual print statement does not append a newline. However, we
                // can only work with ints and bools. Thus to keep output
                // readable we append a newline anyway.
                if (literal.has_value())
                { std::cout << literal.value()->stringify() << std::endl; }
            }
        }
    }
    else
    {
        DefStatement* function = nullptr;
        try
        {
            function = context.lookup_fn(this->id);
        }
        catch (UndefinedIdentifier const& fn_e)
        {
            std::cerr << "\nERROR: ";
            std::cerr << fn_e.what() << std::endl;
            exit(1);
        }

        std::vector<std::optional<Literal*>> params{};
        if (this->expressions)
        {
            auto pair = this->expressions->interpret(std::move(context));
            params    = pair.first;
            context   = std::move(pair.second);
        }

        if (function->arg_list_length() != params.size())
        {
            std::cerr << "\nERROR: Unable to call function "
                             + std::string(function->id) + " with "
                             + std::to_string(params.size())
                             + " arguments. Expected "
                             + std::to_string(function->arg_list_length()) + "."
                      << std::endl;
            exit(1);
        }

        auto pair = function->eval(std::move(context), std::move(params));
        result    = pair.first;
        context   = std::move(pair.second);
    }

    return std::make_pair(result, std::move(context));
}

std::pair<std::optional<Literal*>, ContextTable> BracedExpression::interpret(
    ContextTable&& context)
{
    return this->expression->interpret(std::move(context));
}

std::string SyntaxErrorExpression::stringify(std::string&& accum) const
{
    return accum + "Syntax error nr. " + std::to_string(this->nth);
}

std::string AssignExpression::stringify(std::string&& accum) const
{
    accum += this->id + std::string(" ");
    accum = this->op->stringify(std::move(accum)) + " ";
    return this->expression->stringify(std::move(accum));
}

std::string BinExpression::stringify(std::string&& accum) const
{
    accum = this->left_expression->stringify(std::move(accum)) + " ";
    accum = this->op->stringify(std::move(accum)) + " ";
    return this->right_expression->stringify(std::move(accum));
}

std::string NotExpression::stringify(std::string&& accum) const
{
    accum += "!";
    return this->expression->stringify(std::move(accum));
}

std::string LiteralExpression::stringify(std::string&& accum) const
{
    return this->literal->stringify(std::move(accum));
}

std::string IdentifiedExpression::stringify(std::string&& accum) const
{
    return accum += this->id;
}

std::string NegExpression::stringify(std::string&& accum) const
{
    accum += "-";
    return this->expression->stringify(std::move(accum));
}

std::string IdentifiedBracedExpression::stringify(std::string&& accum) const
{
    accum += this->id;
    accum += "(";

    if (this->expressions)
    { accum = this->expressions->stringify(std::move(accum)); }

    return accum + ")";
}

std::string BracedExpression::stringify(std::string&& accum) const
{
    accum += "(";
    accum = this->expression->stringify(std::move(accum));
    return accum + ")";
}