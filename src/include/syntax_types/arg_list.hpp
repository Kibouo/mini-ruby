#pragma once

struct ArgList
{
    Identifier const* const arg;
    ArgList const* const    other_args;
    inline ArgList(Identifier const* const arg, ArgList const* const other_args)
        : arg(arg), other_args(other_args)
    {
    }
    inline ~ArgList()
    {
        delete this->arg;
        if (this->other_args) delete this->other_args;
    }
    ContextTable interpret(ContextTable&&                        context,
                           std::deque<std::optional<Literal*>>&& params) const;
    std::string  stringify(std::string&& accum = "") const;
    size_t       length() const;
};