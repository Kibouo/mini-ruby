#include "syntax_types.hpp"
#include <iostream>

std::tuple<std::optional<Literal*>, ContextTable, bool>
ElsifConditionAndStatements::interpret(ContextTable&&       context,
                                       Literal const* const case_expr) const
{
    // In if statements we don't have something to match with. To please the
    // compiler we "use" the param anyway.
    (void)case_expr;

    auto       pair = this->condition->interpret(std::move(context));
    auto const condition_result = pair.first.value()->is_true();
    context                     = std::move(pair.second);

    std::optional<Literal*> return_value{};
    if (condition_result)
    {
        auto pair2   = this->statement->interpret(std::move(context));
        return_value = pair2.first;
        context      = std::move(pair2.second);
    }
    else if (this->other_conditions_and_statements)
    {
        return this->other_conditions_and_statements->interpret(
            std::move(context));
    }

    return make_tuple(return_value, std::move(context), condition_result);
}

std::tuple<std::optional<Literal*>, ContextTable, bool>
WhenConditionAndStatements::interpret(ContextTable&&       context,
                                      Literal const* const case_expr) const
{
    auto pair = this->condition->interpret(std::move(context));
    context   = std::move(pair.second);

    std::optional<Literal*> return_value{};
    bool                    condition_result = false;
    try
    {
        auto const comparison = *(pair.first.value()) == *case_expr;
        condition_result      = comparison->is_true();
        delete comparison;
    }
    catch (const std::invalid_argument& e)
    {
        std::cerr << "\nERROR: Failed comparing expressions in when statement."
                  << e.what() << std::endl;
        exit(1);
    }

    if (condition_result)
    {
        auto pair2   = this->statement->interpret(std::move(context));
        return_value = pair2.first;
        context      = std::move(pair2.second);
    }
    else if (this->other_conditions_and_statements)
    {
        return this->other_conditions_and_statements->interpret(
            std::move(context), case_expr);
    }

    return make_tuple(return_value, std::move(context), condition_result);
}

std::string ElsifConditionAndStatements::stringify(std::string&& accum) const
{
    accum += "elsif";
    accum = this->condition->stringify(std::move(accum)) + " ";
    accum += "then ";
    accum = this->statement->stringify(std::move(accum));
    if (this->other_conditions_and_statements)
    {
        accum += "\n";
        return this->other_conditions_and_statements->stringify(
            std::move(accum));
    }
    return std::move(accum);
}

std::string WhenConditionAndStatements::stringify(std::string&& accum) const
{
    accum += "when";
    accum = this->condition->stringify(std::move(accum)) + " ";
    accum += "then ";
    accum = this->statement->stringify(std::move(accum));
    if (this->other_conditions_and_statements)
    {
        return this->other_conditions_and_statements->stringify(
            std::move(accum));
    }
    return std::move(accum);
}