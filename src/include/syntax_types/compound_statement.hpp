#pragma once

struct Statement;

struct CompoundStatement
{
    inline CompoundStatement() {}
    virtual inline ~CompoundStatement() {}
    virtual std::pair<std::optional<Literal*>, ContextTable> interpret(
        ContextTable&& context) const                             = 0;
    virtual std::string stringify(std::string&& accum = "") const = 0;
};

struct HeadCompoundStatement : CompoundStatement
{
    Statement const* const         head_statement;
    CompoundStatement const* const other_statements;
    inline HeadCompoundStatement(
        Statement const* const         head_statement,
        CompoundStatement const* const other_statements)
        : head_statement(head_statement), other_statements(other_statements)
    {
    }
    inline ~HeadCompoundStatement()
    {
        delete this->head_statement;
        if (this->other_statements) delete this->other_statements;
    }
    std::pair<std::optional<Literal*>, ContextTable> interpret(
        ContextTable&& context) const;
    std::string stringify(std::string&& accum = "") const;
};

struct LastCompoundStatement : CompoundStatement
{
    CompoundStatement const* const middle_statements;
    Statement const* const         last_statement;
    inline LastCompoundStatement(
        CompoundStatement const* const middle_statements,
        Statement const* const         last_statement)
        : middle_statements(middle_statements), last_statement(last_statement)
    {
    }
    inline ~LastCompoundStatement()
    {
        delete this->last_statement;
        if (this->middle_statements) delete this->middle_statements;
    }
    std::pair<std::optional<Literal*>, ContextTable> interpret(
        ContextTable&& context) const;
    std::string stringify(std::string&& accum = "") const;
};
