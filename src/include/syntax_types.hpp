#pragma once

#include <deque>
#include <vector>

#include "syntax_types/identifier.hpp"

#include "context_table.hpp"

#include "syntax_types/arg_list.hpp"
#include "syntax_types/assign_op.hpp"
#include "syntax_types/bin_op.hpp"
#include "syntax_types/compound_statement.hpp"
#include "syntax_types/condition_and_statements.hpp"
#include "syntax_types/expression.hpp"
#include "syntax_types/expressions.hpp"
#include "syntax_types/literal.hpp"
#include "syntax_types/program.hpp"
#include "syntax_types/statement.hpp"