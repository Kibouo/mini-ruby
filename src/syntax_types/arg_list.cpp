#include "syntax_types.hpp"
#include <iostream>

ContextTable ArgList::interpret(
    ContextTable&& context, std::deque<std::optional<Literal*>>&& params) const
{
    context.insert(this->arg, params.front().value());
    params.pop_front();
    if (this->other_args)
        context = this->other_args->interpret(std::move(context),
                                              std::move(params));
    return std::move(context);
}

size_t ArgList::length() const
{
    return 1 + (this->other_args ? this->other_args->length() : 0);
}

std::string ArgList::stringify(std::string&& accum) const
{
    accum += this->arg;
    if (this->other_args)
    {
        accum += ", ";
        accum = this->other_args->stringify(std::move(accum));
    }

    return std::move(accum);
}