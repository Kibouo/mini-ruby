#pragma once

enum BinOpEnum
{
    B_PLUS,
    B_MINUS,
    B_MUL,
    B_DIV,
    B_GT,
    B_GE,
    B_LT,
    B_LE,
    B_EQ,
    B_NE,
    B_AND,
    B_OR
};

struct BinOp
{
    BinOpEnum const op;
    inline BinOp(BinOpEnum const op) : op(op) {}
    Literal* interpret(Literal* const val_l, Literal const* const val_r) const;
    std::string stringify(std::string&& accum = "") const;
};
