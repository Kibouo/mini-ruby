#include "syntax_types.hpp"
#include <iostream>

void Program::interpret() const
{
    this->compound_statement->interpret(ContextTable());
}

std::string Program::stringify(std::string&& accum) const
{
    return this->compound_statement->stringify(std::move(accum));
}