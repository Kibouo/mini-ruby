#include "syntax_types.hpp"
#include <iostream>

Literal* AssignOp::interpret(Literal* val_l, Literal const* const val_r) const
{
    try
    {
        switch (this->op)
        {
            case AssignOpEnum::A_ASSIGN:
            {
                *val_l = *val_r;
                return val_l;
            };
            case AssignOpEnum::A_PLUSASSIGN:
            {
                *val_l += *val_r;
                return val_l;
            };
            case AssignOpEnum::A_MINUSASSIGN:
            {
                *val_l -= *val_r;
                return val_l;
            };
            case AssignOpEnum::A_MULASSIGN:
            {
                *val_l *= *val_r;
                return val_l;
            };
            case AssignOpEnum::A_DIVASSIGN:
            {
                *val_l /= *val_r;
                return val_l;
            };
            case AssignOpEnum::A_ANDASSIGN:
            {
                val_l->and_assign_op(*val_r);
                return val_l;
            };
            case AssignOpEnum::A_ORASSIGN:
            {
                val_l->or_assign_op(*val_r);
                return val_l;
            };
        }
    }
    catch (const std::invalid_argument& e)
    {
        throw e;
    }
}

std::string AssignOp::stringify(std::string&& accum) const
{
    switch (this->op)
    {
        case AssignOpEnum::A_ASSIGN: return accum += "=";
        case AssignOpEnum::A_PLUSASSIGN: return accum += "+=";
        case AssignOpEnum::A_MINUSASSIGN: return accum += "-=";
        case AssignOpEnum::A_MULASSIGN: return accum += "*=";
        case AssignOpEnum::A_DIVASSIGN: return accum += "/=";
        case AssignOpEnum::A_ANDASSIGN: return accum += "&&=";
        case AssignOpEnum::A_ORASSIGN: return accum += "||=";
    }
}